[Ч115](http://вики-ч115.программирование-по-русски.рф/%d0%a7115)/

# КомпиляторFox

### Модуль фронтенда

Compiler.Mod /GetOptions<br>
Frontend.Mod /GetFrontendByName ("Oberon")<br>
FoxOberonFrontend.Mod /Frontend.Initialize<br>
Parse [[$[Get Code]]](http://%d0%b2%d0%b8%d0%ba%d0%b8-%d1%87115.%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%b8%d1%80%d0%be%d0%b2%d0%b0%d0%bd%d0%b8%d0%b5-%d0%bf%d0%be-%d1%80%d1%83%d1%81%d1%81%d0%ba%d0%b8.%d1%80%d1%84/%d0%a7115/%d0%9a%d0%be%d0%bc%d0%bf%d0%b8%d0%bb%d1%8f%d1%82%d0%be%d1%80Fox?action=sourceblock&num=1)

### Что-то про символьные файлы и порядок инициализации

```
Compiler.Modules
 FoxBackend.Initialize
 (* Неясно с чего, но мы верим, что используется FoxTextualSymbolFile, впрочем, нам не это важно *)
 FoxTextualSymbolFile.Initialize (?)
 FoxFormats (* могут иметь отношение к симв. файлу *)

  (** generate symbol file **)
  IF (options.symbolFile # NIL) & ~options.symbolFile.Export(module, importCache) =>
   (* Как-то так *)
   FoxTextualSymbolFile.TextualSymbolFile.Export(module:SyntaxTree.Module; importCache: SyntaxTree.ModuleScope)
   Ищем ModuleScope (хотим в него подсунуть константу) - он растёт от просто FoxSyntaxTree.Scope (искать <<Scope=*OBJECT>>)
    PROCEDURE AddConstant - добавляем константу (типа Constant)
```
