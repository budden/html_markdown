[Ч115](http://вики-ч115.программирование-по-русски.рф/%d0%a7115)/

# Сборка

Первоисточник: [http://www.ocp.inf.ethz.ch/wiki/Development/Builds](http://www.ocp.inf.ethz.ch/wiki/Development/Builds) (ссылка уже умерла) , но есть изменения!

## Автоматизированная сборка от команды A2 (сборка из будущего)

tools/builds - так собирают разработчики A2, но я не разбирался и навелосипедил своё. По идее, сборка там описана в makefile, там есть цель $(platform),
она открывает Release.Tool и вызывает команду Release.Build - это компиляция. Но где линковка? Я так и не понял, откуда эта сборка берёт команды линковки и мне кажется, что их там просто нет. Хотя при этом в этот makefile вносятся изменения. Поэтому команды линковки я беру из Builds.Tool, а команды компиляции описаны в Release.Tool

## Моя сборка скриптом (современный способ)

Это - современный способ сборки. Сборка частично интерактивна и наиболее удобна для разработки. Если что-то пошло не так, можно поправить прямо из
среды сборки и перезапустить. На данный момент - [https://gitlab.com/budden/jaos/-/blob/яос032/док/сборка-и-запуск.md](https://gitlab.com/budden/jaos/-/blob/%d1%8f%d0%be%d1%81032/%d0%b4%d0%be%d0%ba/%d1%81%d0%b1%d0%be%d1%80%d0%ba%d0%b0-%d0%b8-%d0%b7%d0%b0%d0%bf%d1%83%d1%81%d0%ba.md)
(ветка часто меняется, а вики не всегда успевает, просто смотри док в свежайших исходниках). Вопреки названию документа, описаны все виды сборке, кроме сборки под железо, которая описана рядом.

## Как собиралось раньше (архивная информация)

Всё нижеследующее не нужно для последних версий, но сохранено, т.к. может понадобиться собрать и старые версии

### Сценарий с помощью wsl-build

Сборка была хрупкой, т.к. модифицировала директорию по всей дороге. Поэтому если ломалась посередине процесса, повторно запустить было проблематично.
На всякий случай лучше копировать весь репозиторий целиком перед попыткой сборки.

Работала через wsl, и далее в нём запускаем

bash /mnt /c /ob /jaos /WinAos /wsl-build /wsl-build.sh                  [[$[Get Code]]](http://%d0%b2%d0%b8%d0%ba%d0%b8-%d1%87115.%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%b8%d1%80%d0%be%d0%b2%d0%b0%d0%bd%d0%b8%d0%b5-%d0%bf%d0%be-%d1%80%d1%83%d1%81%d1%81%d0%ba%d0%b8.%d1%80%d1%84/%d0%a7115/%d0%a1%d0%b1%d0%be%d1%80%d0%ba%d0%b0?action=sourceblock&num=1)

Дальше всё будет написано на экране.

### Собираем яос0 от 2019-12-28

>cd /d C:\ob\jaos\winaos<br>
> move a2.exe a2host.exe<br>
> del * .Dpi * .Obw SystemTrace * .txt<br>
>mkdir obgn<br>
> REM сохраняем куда-нибудь Work\auto.dsk <br>
>rmdir /Q /S Work<br>
>mkdir Work<br>
>mkdir Work /xym<br>
> a2host.exe<br>
> A > PET.Open Release.Tool<br>
> A > Предполагаем, что нужная нам сборка - WinAosNewObjectFile<br>
> A > Находим её текстом и получаем команду (подправим в ней сразу путь):<br>
> REM а тут пришлось скопировать Exclude-ы. <br>
> A > Release.Build --path ="../obgn/" WinAosNewObjectFile ~<br>
> A > Выскакивает файлик с командами. В нём жмём Ctrl-Enter на слове SystemTools в строке SystemTools.DoCommands<br>
> A > А потом вот такая незатейливая команда для линковки (путь тоже поменял)<br>
> A > StaticLinker.Link --fileFormat =PE32 --fileName =A2.exe --extension =GofW --displacement =401000H --path ="../obgn/" Runtime Trace Kernel32 Machine Heaps Modules Objects Kernel KernelLog Streams Commands FIles WinFS Clock Dates Reals Strings Diagnostics BitSets StringPool ObjectFile GenericLinker Reflection  GenericLoader  BootConsole ~<br>
> A > SystemTools.PowerDown ~<br>
> move Work\a2.exe a2.exe<br>
> move Work\a2.log a2.log<br>
> move obg obg.old<br>
> move obgn obg<br>
>rmdir /Q /S xym<br>
>mkdir xym<br>
> move Work\xym\* xym<br>
> a2.exe [[$[Get Code]]](http://%d0%b2%d0%b8%d0%ba%d0%b8-%d1%87115.%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%b8%d1%80%d0%be%d0%b2%d0%b0%d0%bd%d0%b8%d0%b5-%d0%bf%d0%be-%d1%80%d1%83%d1%81%d1%81%d0%ba%d0%b8.%d1%80%d1%84/%d0%a7115/%d0%a1%d0%b1%d0%be%d1%80%d0%ba%d0%b0?action=sourceblock&num=2)

Теоретически дальше надо создать перекрёстные ссылки с помощью команды

TFXRef.MakeXRef "C:/ob/jaos/source/" "*Oberon*"~
, но она заведомо должна сломаться. Кроме того, идея отбирать файлы по маске всё же порочна.
Надо, что ли, переделать, чтобы наконец уже бралось на базе конфигурации, и делалось как часть
сборки.

### Собираем версию от 2016-01-05 для начала. Для неё данные из первоисточника не работают.

* Скачиваем (клон репозитория)
* Заходим в терминале директорию WinAos
* del *.obw
* mkdir objt
* rmdir /Q /S xym
* move aos.exe aosHost.exe
* Запускаем aosHost.exe
* Запускаем блокнот в AOS (Главное меню/Edit/Text)
* Копируем из данной вики-страницы, вставляем в блокнот (Ctrl-W) и выполняем команду:Release.Build --path="objt/" -b WinAos~для выполнения нужно нажать Ctrl-Enter, находясь внутри текста команды.
* То же для команды связывания:PELinker.Link --path=objt/ Win32.Aos.Link~
* Выходим из системы: SystemTools.PowerDown~
* move obj obj.old
* move objt obj
* Может понадобиться повторная пересборка системы из самой себя, например, если механизм

генерации символьных файлов поменялся.

### Собираем a2.exe после 2016-05-15 (новый формат объектных файлов)

>cd /d C:\ob\jaos\winaos<br>
> move a2.exe a2host.exe<br>
>mkdir obgn<br>
>mkdir Work<br>
> a2host.exe<br>
> A > PET.Open Release.Tool<br>
> A > Предполагаем, что нужная нам сборка - WinAosNewObjectFile<br>
> A > Находим её текстом и получаем команду (подправим в ней сразу путь):<br>
> A > Release.Build --path ="../obgn/" WinAosNewObjectFile ~<br>
> A > Выскакивает файлик с командами. В нём жмём Ctrl-Enter на слове SystemTools в строке SystemTools.DoCommands<br>
> A > А потом вот такая незатейливая команда для линковки (путь тоже поменял)<br>
> A > StaticLinker.Link --fileFormat =PE32 --fileName =A2.exe --extension =GofW --displacement =401000H --path ="../obgn/" Runtime Trace Kernel32 Machine Heaps Modules Objects Kernel KernelLog Streams Commands FIles WinFS Clock Dates Reals Strings Diagnostics BitSets StringPool ObjectFile GenericLinker Reflection  GenericLoader  BootConsole ~<br>
> A > SystemTools.PowerDown ~<br>
> move Work\a2.exe a2.exe<br>
> move obg obg.old<br>
> move obgn obg<br>
> a2.exe<br>
# Создаём перекрёстные ссылки<br>
TFXRef.MakeXRef "C:/ob/jaos/source/""*Oberon*" ~[[$[Get Code]]](http://%d0%b2%d0%b8%d0%ba%d0%b8-%d1%87115.%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%b8%d1%80%d0%be%d0%b2%d0%b0%d0%bd%d0%b8%d0%b5-%d0%bf%d0%be-%d1%80%d1%83%d1%81%d1%81%d0%ba%d0%b8.%d1%80%d1%84/%d0%a7115/%d0%a1%d0%b1%d0%be%d1%80%d0%ba%d0%b0?action=sourceblock&num=3)

### Для версии от 2018-11-16, SHA = 1910c44f5feffc8362de3524f7534145ce411c77

* Скачиваем (клон репозитория)
* Заходим в Win32
* mkdir objt
* Запускаем a2.bat
* move oberon.exe oberonHost.exe
* Запускаем Shell
* PET.Open C:/ob/A2OS/source/Release.Tool
* Там ищем WINAOS и в нём второй набор команд
* Выполняем компиляцию, линковку.
* Выходим из системы
* Скопировать все файлы из objt в bin
